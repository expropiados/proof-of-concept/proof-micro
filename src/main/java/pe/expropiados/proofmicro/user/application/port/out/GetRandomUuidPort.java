package pe.expropiados.proofmicro.user.application.port.out;

import java.util.UUID;

public interface GetRandomUuidPort {
    UUID getRandomUuid();
}
