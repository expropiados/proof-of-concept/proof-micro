package pe.expropiados.proofmicro.user.application.port.in;

import pe.expropiados.proofmicro.user.domain.User;

public interface GetUserUseCase {
    User getUser(Long id);
}
