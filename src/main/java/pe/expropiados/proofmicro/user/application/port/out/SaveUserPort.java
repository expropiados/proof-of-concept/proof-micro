package pe.expropiados.proofmicro.user.application.port.out;

import pe.expropiados.proofmicro.user.application.port.in.RegisterUserCommand;
import pe.expropiados.proofmicro.user.domain.User;

import java.util.UUID;

public interface SaveUserPort {
    User saveUser(RegisterUserCommand command, UUID userUuid);
}
