package pe.expropiados.proofmicro.user.application;

import lombok.Getter;
import pe.expropiados.proofmicro.common.hexagonal.errors.UserInputException;

@Getter
public class UserNotFoundException extends RuntimeException implements UserInputException {
    private final String code = "USR_001";
    private final String message;
    private final Long userId;

    public UserNotFoundException(Long userId) {
        super();
        this.userId = userId;
        this.message = String.format("User %d not found", userId);
    }
}
