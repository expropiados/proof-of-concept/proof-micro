package pe.expropiados.proofmicro.user.application.port.in;

import lombok.EqualsAndHashCode;
import lombok.Value;
import pe.expropiados.proofmicro.common.hexagonal.validation.SelfValidating;

import javax.validation.constraints.NotNull;

@Value
@EqualsAndHashCode(callSuper = false)
public class RegisterUserCommand extends SelfValidating<RegisterUserCommand> {
    @NotNull
    String username;

    @NotNull
    String name;

    @NotNull
    String lastname;

    public RegisterUserCommand(String username, String name, String lastname) {
        this.username = username;
        this.name = name;
        this.lastname = lastname;
        this.validateSelf();
    }
}
