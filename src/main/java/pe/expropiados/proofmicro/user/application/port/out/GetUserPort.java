package pe.expropiados.proofmicro.user.application.port.out;

import pe.expropiados.proofmicro.user.domain.User;

import java.util.Optional;

public interface GetUserPort {
    Optional<User> getUser(Long id);
}
