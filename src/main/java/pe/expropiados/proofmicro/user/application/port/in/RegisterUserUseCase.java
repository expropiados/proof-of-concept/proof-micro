package pe.expropiados.proofmicro.user.application.port.in;

import pe.expropiados.proofmicro.user.domain.User;

public interface RegisterUserUseCase {
    User registerUser(RegisterUserCommand command);
}
