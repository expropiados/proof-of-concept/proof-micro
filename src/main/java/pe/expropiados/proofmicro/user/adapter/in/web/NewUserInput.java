package pe.expropiados.proofmicro.user.adapter.in.web;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class NewUserInput {
    private String username;
    private String name;
    private String lastname;
}
