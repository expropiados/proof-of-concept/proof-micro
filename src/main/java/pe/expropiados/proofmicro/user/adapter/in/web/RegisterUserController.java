package pe.expropiados.proofmicro.user.adapter.in.web;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pe.expropiados.proofmicro.common.hexagonal.WebAdapter;
import pe.expropiados.proofmicro.user.application.port.in.RegisterUserCommand;
import pe.expropiados.proofmicro.user.application.port.in.RegisterUserUseCase;
import pe.expropiados.proofmicro.user.domain.User;

@WebAdapter
@RestController
@RequiredArgsConstructor
public class RegisterUserController {
    private final RegisterUserUseCase registerUserUseCase;

    @PostMapping("/users")
    public User registerUser(@RequestBody RegisterUserCommand command) {
        return registerUserUseCase.registerUser(command);
    }
}
