package pe.expropiados.proofmicro.user.adapter.out.persistence;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.stereotype.Component;
import pe.expropiados.proofmicro.user.application.port.in.RegisterUserCommand;
import pe.expropiados.proofmicro.user.domain.User;

@Mapper(componentModel="spring")
@Component
public interface UserMapper {

    @Mapping(source = "id", target = "id")
    @Mapping(source = "name", target = "name")
    @Mapping(source = "lastname", target = "lastname")
    @Mapping(source = "username", target = "username")
    @Mapping(source = "uuid", target = "uuid")
    User toUser(UserJpaEntity userJpaEntity);

    @InheritInverseConfiguration
    UserJpaEntity toUserJpaEntity(User user);

    @Mapping(target = "id", ignore = true)
    @Mapping(source = "name", target = "name")
    @Mapping(source = "lastname", target = "lastname")
    @Mapping(source = "username", target = "username")
    @Mapping(target = "uuid", ignore = true)
    UserJpaEntity toUserJpaEntity(RegisterUserCommand command);
}
