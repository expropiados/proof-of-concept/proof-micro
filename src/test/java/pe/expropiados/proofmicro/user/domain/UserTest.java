package pe.expropiados.proofmicro.user.domain;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("UnitTest")
public class UserTest {

    @Test
    void getFullNameSuccess() {
        var idx = 1L;
        var uuid = UUID.randomUUID();
        var username = "Caricato";
        var name = "Diego";
        var lastname = "Villegas";

        User user = new User(idx, uuid, username, name, lastname);
        assertThat(user.getFullName()).isEqualTo("Diego Villegas");
    }
}
