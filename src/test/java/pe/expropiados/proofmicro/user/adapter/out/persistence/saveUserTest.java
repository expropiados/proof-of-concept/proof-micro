package pe.expropiados.proofmicro.user.adapter.out.persistence;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import pe.expropiados.proofmicro.user.application.port.in.RegisterUserCommand;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@Import({UserPersistenceAdapter.class})
@ComponentScan(basePackageClasses = {UserMapper.class})
@ContextConfiguration
class saveUserTest {
    @Autowired
    private UserPersistenceAdapter userPersistenceAdapterTest;


    @Test
    void saveUser(){
        var registerUser = new RegisterUserCommand("Test", "Diego", "Villegas");

        var user = userPersistenceAdapterTest.saveUser(registerUser, UUID.randomUUID());
        assertThat(user).isNotNull();
        assertThat(user.getName()).isEqualTo(registerUser.getName());
        assertThat(user.getLastname()).isEqualTo(registerUser.getLastname());
        assertThat(user.getUsername()).isEqualTo(registerUser.getUsername());

        assertThat(user.getId()).isNotNull();
    }
}
