package pe.expropiados.proofmicro.user.adapter.out.persistence;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;


import static org.assertj.core.api.Assertions.assertThat;

@Tag("IntegrationTest")
@DataJpaTest
@Import({UserPersistenceAdapter.class})
@ComponentScan(basePackageClasses = {UserMapper.class})
@ContextConfiguration
class getUserTest {

    @Autowired
    private UserPersistenceAdapter userPersistenceAdapterTest;



    @Test
    @Sql("getUserTest.sql")
    void loadUser(){
        var userSuccess = userPersistenceAdapterTest.getUser(2L);
        assertThat(userSuccess).isPresent();
        assertThat(userSuccess.get().getId()).isEqualTo(2L);
        assertThat(userSuccess.get().getName()).isEqualTo("Marco");
        assertThat(userSuccess.get().getLastname()).isEqualTo("Quezada");
        assertThat(userSuccess.get().getUsername()).isEqualTo("Matrix");

        var userFail = userPersistenceAdapterTest.getUser(10L);
        assertThat(userFail).isEmpty();
    }
}
