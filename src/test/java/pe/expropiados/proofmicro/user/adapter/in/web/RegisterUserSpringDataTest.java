package pe.expropiados.proofmicro.user.adapter.in.web;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import pe.expropiados.proofmicro.user.application.port.in.RegisterUserCommand;
import pe.expropiados.proofmicro.user.application.port.in.RegisterUserUseCase;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = RegisterUserController.class)
public class RegisterUserSpringDataTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RegisterUserUseCase registerUserUseCase;

    @Test
    void testRegisterUser() throws Exception{
        RegisterUserCommand registerUserCommand = new RegisterUserCommand("admin1",
                "admin", "admin");
        String params = "{ \"lastname\": \"admin\", \"name\": \"admin\", \"username\": \"admin1\"}";
        mockMvc.perform(post("/users").
                content(params).
                header("Content-Type", "application/json")).andExpect(status().isOk());

        then(registerUserUseCase).should()
                .registerUser(registerUserCommand);
    }
}
