package pe.expropiados.proofmicro.user.adapter.in.web;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import pe.expropiados.proofmicro.user.adapter.in.web.GetUserController;
import pe.expropiados.proofmicro.user.application.port.in.GetUserUseCase;

import static org.mockito.BDDMockito.then;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Tag("IntegrationTest")
@WebMvcTest(controllers = GetUserController.class)
public class GetUserSpringDataTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private GetUserUseCase getUserUseCase;

    @Test
    void testGetUser() throws Exception{
        mockMvc.perform(get("/users/{id}", 1L).
                header("Content-Type", "application/json")).andExpect(status().isOk());

        then(getUserUseCase).should()
                .getUser(1L);
    }
}
