package pe.expropiados.proofmicro.user.application;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import pe.expropiados.proofmicro.user.application.port.out.GetUserPort;
import pe.expropiados.proofmicro.user.domain.User;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@Tag("UnitTest")
public class GetUserTest {
    private GetUserPort getUserPort;
    private GetUserService getUserService;

    User getRandomUser() {
        var idx = 1L;
        var uuid = UUID.randomUUID();
        var username = "Caricato";
        var name = "Diego";
        var lastname = "Villegas";

        return new User(idx, uuid, username, name, lastname);
    }

    @BeforeEach
    void init() {
        this.getUserPort = Mockito.mock(GetUserPort.class);
        this.getUserService = new GetUserService(this.getUserPort);
    }

    @Test
    void shouldFailWithUserNotFound_whenUserDoesntExist() {
        when(getUserPort.getUser(anyLong())).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> getUserService.getUser(1L));
    }

    @Test
    void shouldReturnUser_whenUserExists() {
        var user = getRandomUser();
        when(getUserPort.getUser(anyLong())).thenReturn(Optional.of(user));

        var userReturned = getUserService.getUser(1L);

        assertEquals(user.getId(), userReturned.getId());
        assertEquals(user.getName(), userReturned.getName());
        assertEquals(user.getUsername(), userReturned.getUsername());
        assertEquals(user.getLastname(), userReturned.getLastname());
        assertEquals(user.getUuid(), userReturned.getUuid());
    }
}
