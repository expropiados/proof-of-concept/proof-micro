package pe.expropiados.proofmicro.user.application.port.in.configuration;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import pe.expropiados.proofmicro.user.application.port.in.RegisterUserCommand;
import pe.expropiados.proofmicro.user.domain.User;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class UserHttpClientTest {
    private final String SERVER_URL = "http://localhost";
    private final String APP_CONTEXT_URL = "/proof-micro/api";
    private final String THINGS_ENDPOINT = "/users";

    @LocalServerPort
    private int port;
    private final TestRestTemplate restTemplate = new TestRestTemplate();

    private String thingsEndpoint() {
        return SERVER_URL + ":" + port + APP_CONTEXT_URL+ THINGS_ENDPOINT;
    }

    public int put(final Object something) {
        return restTemplate.postForEntity(thingsEndpoint(), something, Object.class).getStatusCodeValue();
    }

    public Object getContents(int id) {
        return restTemplate.getForEntity(thingsEndpoint()+"/"+id, Object.class).getBody();
    }

    public void clean() {
        restTemplate.delete(thingsEndpoint());
    }
}
