package pe.expropiados.proofmicro.user.application.port.in.runner;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pe.expropiados.proofmicro.user.application.port.in.RegisterUserCommand;
import pe.expropiados.proofmicro.user.application.port.in.configuration.UserHttpClientTest;
import pe.expropiados.proofmicro.user.domain.User;

import static org.assertj.core.api.Assertions.assertThat;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GetUserStepDefinitions {

    private static final String USER_RESOURCE_URL = "/users";
    private Object object;
    @Autowired
    private UserHttpClientTest userHttpClient;


    @Given("the Admin register a user")
    public void the_Admin_register_a_user() {
        var registerUser = new RegisterUserCommand("Test", "Diego", "Villegas");
        var status = userHttpClient.put(registerUser);
        assertThat(status).isEqualTo(200);
    }

    @When("the Admin asks for his registered user")
    public void the_Admin_asks_for_his_registered_user() {
        object = userHttpClient.getContents(1);
    }

    @Then("the Admin gets his user")
    public void the_Admin_gets_his_user() {
        System.out.println(object.toString());
    }
}
