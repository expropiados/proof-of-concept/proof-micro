package pe.expropiados.proofmicro.user.application.port.in.runner;

import org.junit.runner.RunWith;
import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.jupiter.api.Tag;

@Tag("E2ETest")
@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome = true,
        features = "classpath:features",
        extraGlue = "pe.expropiados.proofmicro.user.application.port.in.configuration"
)
public class GetUserUseCaseTest {
}
